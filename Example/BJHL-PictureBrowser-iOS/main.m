//
//  main.m
//  BJHL-PictureBrowser-iOS
//
//  Created by yanxiaoliang on 11/27/2015.
//  Copyright (c) 2015 yanxiaoliang. All rights reserved.
//

@import UIKit;
#import "BJCPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BJCPAppDelegate class]));
    }
}
