//
//  BJCPAppDelegate.h
//  BJHL-PictureBrowser-iOS
//
//  Created by yanxiaoliang on 11/27/2015.
//  Copyright (c) 2015 yanxiaoliang. All rights reserved.
//

@import UIKit;

@interface BJCPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
