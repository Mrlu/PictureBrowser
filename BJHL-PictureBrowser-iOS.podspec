#
# Be sure to run `pod lib lint BJHL-PictureBrowser-iOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BJHL-PictureBrowser-iOS"
  s.version          = "1.0.0"
  s.summary          = "A short description of BJHL-PictureBrowser-iOS."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                       DESC

  s.homepage         = "http://git.baijiahulian.com/iOS/BJHL-PictureBrowser-iOS"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "yanxiaoliang" => "yanxiaoliang@baijiahulian.com" }
  s.source           = { :git => "git@git.baijiahulian.com:iOS/BJHL-PictureBrowser-iOS.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '6.1'
  s.requires_arc = true

  s.dependency 'TKAlert&TKActionSheet'
  s.dependency 'SDWebImage'
  s.dependency 'MBProgressHUD'

  s.source_files = 'Pod/Classes/**/*'

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit', 'MessageUI'
end
