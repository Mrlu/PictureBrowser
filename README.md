# BJHL-PictureBrowser-iOS

[![CI Status](http://img.shields.io/travis/yanxiaoliang/BJHL-PictureBrowser-iOS.svg?style=flat)](https://travis-ci.org/yanxiaoliang/BJHL-PictureBrowser-iOS)
[![Version](https://img.shields.io/cocoapods/v/BJHL-PictureBrowser-iOS.svg?style=flat)](http://cocoapods.org/pods/BJHL-PictureBrowser-iOS)
[![License](https://img.shields.io/cocoapods/l/BJHL-PictureBrowser-iOS.svg?style=flat)](http://cocoapods.org/pods/BJHL-PictureBrowser-iOS)
[![Platform](https://img.shields.io/cocoapods/p/BJHL-PictureBrowser-iOS.svg?style=flat)](http://cocoapods.org/pods/BJHL-PictureBrowser-iOS)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.
实现了图片的浏览大图功能

## Requirements

## Installation

BJHL-PictureBrowser-iOS is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "BJHL-PictureBrowser-iOS"
```

## Author

yanxiaoliang, yanxiaoliang@baijiahulian.com

## License

BJHL-PictureBrowser-iOS is available under the MIT license. See the LICENSE file for more info.
